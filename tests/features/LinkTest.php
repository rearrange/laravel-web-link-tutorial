<?php

/* 
 * The MIT License
 *
 * Copyright 2016 rearrange.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

use Illuminate\Foundation\Testing\WithoutMiddleware;

class LinkTest extends TestCase
{
    public function testWeSeeAListOfLinks()
    {
        factory(App\Link::class)->create([
            'title' => 'dotdev.co',            
        ]);
        
        $this->visit('/')
                ->see('dotdev.co');
    }
    
    public function testWeSeeLinksForm()
    {
        $this->visit('/submit')
                ->see('Submit a Link');
    }
    
    public function testLinksFormValidation()
    {
        $this->visit('/submit')
             ->press('Submit')
                ->see('The title field is required')
                ->see('The url field is required')
                ->see('The description field is required');        
    }
    
    public function testSubmitLinksToDB()
    {
        $this->visit('/submit')
                ->type('dotdev', 'title')
                ->type('https://dotdev.co', 'url')
                ->type('My description', 'description')
                ->press('Submit')
                ->seeInDatabase('links', ['title' => 'dotdev']);
    }
}