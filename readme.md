# Laravel PHP Framework Tutorial: Simple Link List

This is my repository when I learn Laravel following this [tutorial](https://dotdev.co/step-by-step-guide-to-building-your-first-laravel-application/), where I build a simple link list. 

Version of Laravel used in this tutorial: v5.2

# Installation

Clone this repo, put inside [Homestead](https://laravel.com/docs/master/homestead), access it via your specified link.  
